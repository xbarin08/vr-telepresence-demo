#include "openglwidget.h"
#include <QCoreApplication>
#include <QMutexLocker>
#include <QOpenGLShader>
#include <QResizeEvent>
#include <algorithm>

static QString const vertexShaderSource =
    QString::fromLatin1("#version 330 core\n"

                        "layout(location=0) in vec2 position;\n"
                        "layout(location=1) in vec2 texc;\n"

                        "out vec4 texcoord;\n"

                        "uniform mat4 MVP;\n"

                        "void main()\n"
                        "{\n"
                        "    gl_Position = MVP * vec4(position, 0, 1);\n"
                        "    texcoord = vec4(texc, 0, 1);\n"
                        "}\n");

static QString const fragmentShaderSource =
    QString::fromLatin1("#version 330 core\n"

                        "out vec4 fcolor;\n"
                        "in vec4 texcoord;\n"

                        "uniform sampler2D tex;\n"

                        "void main()\n"
                        "{\n"
                        "    fcolor = vec4(texture(tex, texcoord.xy).rgb, 1);\n"
                        "    //fcolor = vec4(texcoord.x, texcoord.y, 0, 1);\n"
                        "}\n");

static GLfloat const vertices_tex[16] = {
    // clang-format off
     1.0f,  1.0f, 1.0f, 1.0f, // top-right
    -1.0f,  1.0f, 0.0f, 1.0f, // top-left
     1.0f, -1.0f, 1.0f, 0.0f, // bottom-right
    -1.0f, -1.0f, 0.0f, 0.0f  // bottom-left
    // clang-format on
};

OpenGLWidget::OpenGLWidget(QWidget *parent) : QOpenGLWidget(parent)
{
    oldSize = size();
}

OpenGLWidget::~OpenGLWidget()
{
    makeCurrent();
    logger.stopLogging();
    texLeft.destroy();
    texRight.destroy();
}

void OpenGLWidget::paintGL()
{
    vr->update();
    vao.bind();
    shader.bind();
    glActiveTexture(GL_TEXTURE0);
    glViewport(0, 0, size().width(), size().height());

    if (toUpdate)
    {
        loadTexture(tmpImageLeft, texLeft);
        loadTexture(tmpImageRight, texRight);

        newSize =
            QSize(tmpImageLeft.width() + tmpImageRight.width(),
                  std::max(tmpImageLeft.height(), tmpImageRight.height()));

        newSize = newSize / 2;
        if (oldSize != newSize)
        {
            QCoreApplication::postEvent(this,
                                        new QResizeEvent(newSize, oldSize));
            oldSize = newSize;

            float vrAspect = static_cast<float>(fboLeft->width()) /
                             static_cast<float>(fboLeft->height());
            float cameraAspect = static_cast<float>(tmpImageLeft.width()) /
                                 static_cast<float>(tmpImageLeft.height());

            float frameWidth  = 1;
            float frameHeight = 1;
            if (cameraAspect > vrAspect)
            {
                frameHeight = vrAspect / cameraAspect;
            }
            else if (cameraAspect < vrAspect)
            {
                frameWidth = cameraAspect / vrAspect;
            }
            QMatrix4x4 tmpmat;
            tmpmat.scale(frameWidth, frameHeight);
            vrTransform = tmpmat;
        }
    }

    shader.setUniformValue(MVPUL, vrTransform);

    fboLeft->bind();
    texLeft.bind();
    glViewport(0, 0, fboLeft->width(), fboLeft->height());
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    vr->submitLeftEye(fboLeft->texture());
    fboRight->bind();
    texRight.bind();
    glViewport(0, 0, fboLeft->width(), fboLeft->height());
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    vr->submitRightEye(fboRight->texture());
    texRight.release();
    fboRight->bindDefault();

    shader.setUniformValue(MVPUL, QMatrix4x4());

    texLeft.bind();
    glViewport(0, 0, width() / 2, height());
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    texRight.bind();
    glViewport(width() / 2, 0, width() / 2, height());
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    texRight.release();

    vr->PostPresentHandoff();

    shader.release();
    vao.release();
    toUpdate = false;
}

void OpenGLWidget::resizeGL(int w, int h)
{
    QOpenGLWidget::resizeGL(w, h);
}

void OpenGLWidget::initializeGL()
{
    initializeOpenGLFunctions();
    initFrameBuffers(vr->getResolution());
    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    logger.initialize();
    logger.startLogging();
    logger.enableMessages(QOpenGLDebugMessage::AnySource,
                          QOpenGLDebugMessage::AnyType,
                          QOpenGLDebugMessage::AnySeverity);
    vao.create();
    vao.bind();
    QOpenGLShader vertexShader(QOpenGLShader::Vertex);
    vertexShader.compileSourceCode(vertexShaderSource);

    QOpenGLShader fragmentShader(QOpenGLShader::Fragment);
    fragmentShader.compileSourceCode(fragmentShaderSource);

    shader.addShader(&vertexShader);
    shader.addShader(&fragmentShader);
    shader.link();
    shader.bind();

    MVPUL = shader.uniformLocation("MVP");
    texUL = shader.uniformLocation("tex");
    glActiveTexture(GL_TEXTURE0);
    shader.setUniformValue(texUL, 0);

    vbo.create();
    vbo.bind();
    vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    vbo.allocate(vertices_tex, sizeof(vertices_tex));

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (4 * sizeof(float)),
                          nullptr);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (4 * sizeof(float)),
                          reinterpret_cast<void *>(2 * sizeof(float)));

    texLeft.create();
    texLeft.setMinMagFilters(QOpenGLTexture::Linear, QOpenGLTexture::Linear);
    texLeft.setWrapMode(QOpenGLTexture::ClampToEdge);
    glBindTexture(GL_TEXTURE_2D, texLeft.textureId());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 100, 100, 0, GL_BGRA,
                 GL_UNSIGNED_BYTE, nullptr);
    glBindTexture(GL_TEXTURE_2D, 0);

    texRight.create();
    texRight.setMinMagFilters(QOpenGLTexture::Linear, QOpenGLTexture::Linear);
    texRight.setWrapMode(QOpenGLTexture::ClampToEdge);
    glBindTexture(GL_TEXTURE_2D, texRight.textureId());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 100, 100, 0, GL_BGRA,
                 GL_UNSIGNED_BYTE, nullptr);
    glBindTexture(GL_TEXTURE_2D, 0);

    vbo.release();
    shader.release();
    vao.release();
}

void OpenGLWidget::loadTexture(const QImage &img, const QOpenGLTexture &tex)
{
    GLenum format = GL_BGR;
    GLenum type   = GL_UNSIGNED_BYTE;

    if (img.format() == QImage::Format_RGB888)
    {
        format = GL_BGR;
        type   = GL_UNSIGNED_BYTE;
    }
    else if (img.format() == QImage::Format_RGBA8888)
    {
        format = GL_BGRA;
        type   = GL_UNSIGNED_BYTE;
    }
    else if (img.format() == QImage::Format_Grayscale8)
    {
        format = GL_RED;
        type   = GL_UNSIGNED_BYTE;
    }

    glBindTexture(GL_TEXTURE_2D, tex.textureId());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img.width(), img.height(), 0, format,
                 type, nullptr);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img.width(), img.height(), 0, format,
                 type, img.constBits());
    glBindTexture(GL_TEXTURE_2D, 0);
}

void OpenGLWidget::setVr(CHMD *value)
{
    vr = value;
}

void OpenGLWidget::updateTextureLeft(QImage *img)
{
    tmpImageLeft = *img;
    toUpdate     = true;
}

void OpenGLWidget::updateTextureRight(QImage *img)
{
    tmpImageRight = *img;
}

QSize OpenGLWidget::sizeHint() const
{
    return newSize;
}

QSize OpenGLWidget::minimumSizeHint() const
{
    return newSize;
}

void OpenGLWidget::initFrameBuffers(QSize size)
{
    makeCurrent();
    fboLeft.reset();
    fboRight.reset();
    fboLeft  = std::make_unique<QOpenGLFramebufferObject>(size, GL_TEXTURE_2D);
    fboRight = std::make_unique<QOpenGLFramebufferObject>(size, GL_TEXTURE_2D);
}

void OpenGLWidget::on_logger_logged(const QOpenGLDebugMessage &message)
{
    qDebug() << message;
}
