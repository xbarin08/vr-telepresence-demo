#include "cframegrabber.h"
#include <utility>

CFrameGrabber::CFrameGrabber(QImage *const img,
                             std::function<void(void)> callback)
    : image(img), callback(std::move(callback))
{
}

void CFrameGrabber::frameReady(DShowLib::Grabber &caller,
                               smart_ptr<DShowLib::MemBuffer> buffer,
                               DWORD FrameNumber)
{
    buffer->lock();
    // auto bitsPerPix   = buffer->getBitsPerPixel();
    auto colorFormat = buffer->getColorformat();
    auto frameType   = buffer->getFrameType();
    auto frameSize   = frameType.dim;
    // auto bitmapHeader = DShowLib::createBitmapInfoHeader(frameType);
    // setGeometry(0, 0, size.cx, size.cy);
    auto newSize = QSize(frameSize.cx, frameSize.cy);

    auto imgData = reinterpret_cast<uchar *>(buffer->getPtr()); // BGRA !!!!!

    QImage::Format imageFormat;

    if (colorFormat == DShowLib::tColorformatEnum::eRGB32)
    {
        imageFormat = QImage::Format_RGBA8888;
    }
    else if (colorFormat == DShowLib::tColorformatEnum::eRGB24)
    {
        imageFormat = QImage::Format_RGB888;
    }
    else if (colorFormat == DShowLib::tColorformatEnum::eY800)
    {
        imageFormat = QImage::Format_Grayscale8;
    }
    else
    {
        imageFormat = QImage::Format_Grayscale8;
    }

    *image = QImage(imgData, newSize.width(), newSize.height(), imageFormat);
    buffer->unlock();
    callback();
}
