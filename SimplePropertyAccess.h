#ifndef SIMPLEPROPERTYACCESS_H_INC_
#define SIMPLEPROPERTYACCESS_H_INC_

#include "tisudshl.h"

class CSimplePropertyAccess
{
public:
    CSimplePropertyAccess();
    explicit CSimplePropertyAccess(
        const _DSHOWLIB_NAMESPACE::tIVCDPropertyItemsPtr &pItems);
    ~CSimplePropertyAccess();

    void init(const _DSHOWLIB_NAMESPACE::tIVCDPropertyItemsPtr &pItems);

    bool isAvailable(GUID id);
    long getValue(GUID id);
    void setValue(GUID id, long val);

    long getRangeMin(GUID id);
    long getRangeMax(GUID id);
    long getDefault(GUID id);

    bool isAutoAvailable(GUID id);
    bool getAuto(GUID id);
    void setAuto(GUID id, bool b);

    bool isSwitchAvailable(GUID id);
    bool getSwitch(GUID id);
    void setSwitch(GUID id, bool b);

    bool isOnePushAvailable(GUID id);
    void push(GUID id);

protected:
    _DSHOWLIB_NAMESPACE::tIVCDPropertyItemsPtr m_pItemContainer;
};

#endif // SIMPLEPROPERTYACCESS_H_INC_
