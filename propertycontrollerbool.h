#ifndef PROPERTYCONTROLLERBOOL_H
#define PROPERTYCONTROLLERBOOL_H

#include "tisudshl.h"
#include <QCheckBox>

class PropertyControllerBool : public QCheckBox
{
    Q_OBJECT

public:
    explicit PropertyControllerBool(QWidget *parent = nullptr);
    explicit PropertyControllerBool(const QString &text,
                                    QWidget *parent = nullptr);

    bool getValue() const;
    void setValue(bool val);
    bool getAuto() const;

    void loadProperties(DShowLib::Grabber *grabber, GUID id);
    void refreshProperties(DShowLib::Grabber *grabber, GUID id);
    void applyProperties(DShowLib::Grabber *grabber, GUID id);

private:
    void setPossible(bool b);
};

#endif // PROPERTYCONTROLLERBOOL_H
