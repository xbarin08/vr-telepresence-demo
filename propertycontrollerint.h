#ifndef PROPERTYCONTROLLERINT_H
#define PROPERTYCONTROLLERINT_H

#include "tisudshl.h"
#include <QGroupBox>

namespace Ui
{
class PropertyControllerInt;
}

class PropertyControllerInt : public QGroupBox
{
    Q_OBJECT

public:
    explicit PropertyControllerInt(QWidget *parent = nullptr);
    ~PropertyControllerInt() override;

    int getValue() const;
    void setValue(int val);
    bool getAuto() const;

    void loadProperties(DShowLib::Grabber *grabber, GUID id);
    void refreshProperties(DShowLib::Grabber *grabber, GUID id);
    void applyProperties(DShowLib::Grabber *grabber, GUID id);

signals:

    void valueChanged(int value);
    void autoChanged(bool b);

private:
    Ui::PropertyControllerInt *ui;

    void setRange(int min, int max);
    void setAutoPossible(bool b);
    void setPossible(bool b);
    void setAuto(bool b);
};

#endif // PROPERTYCONTROLLERINT_H
