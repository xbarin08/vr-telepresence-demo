#ifndef CAMERAPROPERTIES_H
#define CAMERAPROPERTIES_H

#include "tisudshl.h"
#include <QGroupBox>

namespace Ui
{
class CameraProperties;
}

class CameraProperties : public QGroupBox
{
    Q_OBJECT

public:
    explicit CameraProperties(QWidget *parent = nullptr);
    ~CameraProperties() override;

    void loadProperties(DShowLib::Grabber *grabber);
    void refreshProperties(DShowLib::Grabber *grabber);

public slots:

    void applyProperties(DShowLib::Grabber *grabber);

private:
    Ui::CameraProperties *ui;
};

#endif // CAMERAPROPERTIES_H
