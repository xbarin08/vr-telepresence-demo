#ifndef DEVICESELECTOR_H
#define DEVICESELECTOR_H

#include "tisudshl.h"
#include <QGroupBox>
#include <QMap>

namespace Ui
{
class DeviceSelector;
}

class DeviceSelector : public QGroupBox
{
    Q_OBJECT

public:
    explicit DeviceSelector(QWidget *parent = nullptr);
    ~DeviceSelector() override;

    void deviceSelect(DShowLib::Grabber *tmpGrabber);

    void setGrabber(DShowLib::Grabber *value);

public slots:

    void devicesRefresh();

private slots:
    void on_comboDevs_currentIndexChanged(const QString &arg1);

    void on_comboNorms_currentIndexChanged(int index);

    void on_comboFormats_currentIndexChanged(int index);

    void on_pushButton_clicked();

private:
    Ui::DeviceSelector *ui;

    QMap<QString, DShowLib::VideoCaptureDeviceItem> availableDevices;
    QMap<QString, DShowLib::VideoNormItem> availableVideoNorms;
    QMap<QString, smart_ptr<DShowLib::VideoFormatDesc>> availableVideoFormats;

    DShowLib::Grabber *grabber{};
};

#endif // DEVICESELECTOR_H
