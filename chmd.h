#ifndef CHMD_H
#define CHMD_H

#include <QObject>
#include <QOpenGLFunctions>
#include <QSize>
#include <openvr.h>

class CHMD : public QObject
{
    Q_OBJECT
public:
    explicit CHMD(QObject *parent = nullptr);
    ~CHMD() override;
    void initOpenVR();

    void update();

    void submitLeftEye(GLuint target);
    void submitRightEye(GLuint target);
    void PostPresentHandoff();
    QSize getResolution();

signals:

public slots:

private:
    void processVREvents();
    void updatePoses();
    void submitImage(GLuint target, vr::EVREye eye);

    vr::IVRSystem *hmd{nullptr};
    vr::IVRCompositor *compositor{nullptr};
    vr::TrackedDevicePose_t trackedDevicePose[vr::k_unMaxTrackedDeviceCount]{};
};

#endif // CHMD_H
