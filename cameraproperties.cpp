#include "cameraproperties.h"
#include "ui_cameraproperties.h"

CameraProperties::CameraProperties(QWidget *parent)
    : QGroupBox(parent), ui(new Ui::CameraProperties)
{
    ui->setupUi(this);
}

CameraProperties::~CameraProperties()
{
    delete ui;
}

void CameraProperties::loadProperties(DShowLib::Grabber *grabber)
{
    ui->groupExposure->loadProperties(grabber, DShowLib::VCDID_Exposure);
    ui->groupGain->loadProperties(grabber, DShowLib::VCDID_Gain);
    ui->checkHighlight->loadProperties(grabber,
                                       DShowLib::VCDID_Highlightreduction);
}

void CameraProperties::refreshProperties(DShowLib::Grabber *grabber)
{
    ui->groupExposure->refreshProperties(grabber, DShowLib::VCDID_Exposure);
    ui->groupGain->refreshProperties(grabber, DShowLib::VCDID_Gain);
    ui->checkHighlight->refreshProperties(grabber,
                                          DShowLib::VCDID_Highlightreduction);
}

void CameraProperties::applyProperties(DShowLib::Grabber *grabber)
{
    ui->groupExposure->applyProperties(grabber, DShowLib::VCDID_Exposure);
    ui->groupGain->applyProperties(grabber, DShowLib::VCDID_Gain);
    ui->checkHighlight->applyProperties(grabber,
                                        DShowLib::VCDID_Highlightreduction);
}
