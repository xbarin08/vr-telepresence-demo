#include "cimagewindow.h"
#include "ui_cimagewindow.h"
#include <QColor>
#include <QResizeEvent>
#include <Windows.h>

CImageWindow::CImageWindow(QWidget *parent)
    : QWidget(parent), ui(new Ui::CImageWindow),
      grabberLeft(const_cast<QImage *const>(&imageLeft),
                  std::bind(&CImageWindow::leftFrameReady, this)),
      grabberRight(const_cast<QImage *const>(&imageRight),
                   std::bind(&CImageWindow::rightFrameReady, this))
{
    ui->setupUi(this);
    connect(ui->opelGLWidget, &OpenGLWidget::resized, this,
            &CImageWindow::on_widget_resize);
    vr.initOpenVR();
    ui->opelGLWidget->setVr(&vr);
}

CImageWindow::~CImageWindow()
{
    delete ui;
}

void CImageWindow::leftFrameReady()
{
    ui->opelGLWidget->updateTextureLeft(&imageLeft);
    ui->opelGLWidget->update();
}

void CImageWindow::rightFrameReady()
{
    ui->opelGLWidget->updateTextureRight(&imageRight);
    ui->opelGLWidget->update();
}

CFrameGrabber *CImageWindow::getGrabberRight()
{
    return &grabberRight;
}

CFrameGrabber *CImageWindow::getGrabberLeft()
{
    return &grabberLeft;
}

void CImageWindow::on_pushStop_clicked()
{
    emit stop();
}

void CImageWindow::on_widget_resize()
{
    updateGeometry();
}
