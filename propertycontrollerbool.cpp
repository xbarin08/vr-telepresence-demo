#include "propertycontrollerbool.h"
#include "SimplePropertyAccess.h"
#include <QtDebug>

PropertyControllerBool::PropertyControllerBool(QWidget *parent)
    : QCheckBox(parent)
{
}

PropertyControllerBool::PropertyControllerBool(const QString &text,
                                               QWidget *parent)
    : QCheckBox(text, parent)
{
}

bool PropertyControllerBool::getValue() const
{
    return isChecked();
}

void PropertyControllerBool::setValue(bool val)
{
    setChecked(val);
}

bool PropertyControllerBool::getAuto() const
{
    return false;
}

void PropertyControllerBool::loadProperties(DShowLib::Grabber *grabber, GUID id)
{
    if (!(grabber->getAvailableVCDProperties().get()))
    {
        qDebug() << "cannot get VCD properties: "
                 << grabber->getDev().toString().c_str() << " "
                 << grabber->getLastError().toString().c_str();
        return;
    }
    CSimplePropertyAccess prop(grabber->getAvailableVCDProperties());
    if (prop.isAvailable(id))
    {
        setPossible(true);
        //        setAutoPossible(prop.isAutoAvailable(id));
        //        setAuto(prop.getAuto(id));
        //        setRange(prop.getRangeMin(id), prop.getRangeMax(id));
        setValue(prop.getValue(id) != 0);
    }
    else
    {
        setPossible(false);
    }
}

void PropertyControllerBool::refreshProperties(DShowLib::Grabber *grabber,
                                               GUID id)
{
    CSimplePropertyAccess prop(grabber->getAvailableVCDProperties());
    if (prop.isAvailable(id))
    {
        if (prop.getAuto(id))
        {
            //            setAutoPossible(prop.isAutoAvailable(id));
            //            setAuto(prop.getAuto(id));
            //            setRange(prop.getRangeMin(id), prop.getRangeMax(id));
            setValue(prop.getValue(id) != 0);
        }
    }
}

void PropertyControllerBool::applyProperties(DShowLib::Grabber *grabber,
                                             GUID id)
{
    if (!grabber->getAvailableVCDProperties().get())
    {
        qDebug() << "cannot load VCD properties: "
                 << grabber->getLastError().toString().c_str();
        return;
    }
    CSimplePropertyAccess prop(grabber->getAvailableVCDProperties());
    if (!prop.isAvailable(id))
    {
        return;
    }
    prop.setValue(id, isChecked() ? 1 : 0);
}

void PropertyControllerBool::setPossible(bool b)
{
    setEnabled(b);
}
