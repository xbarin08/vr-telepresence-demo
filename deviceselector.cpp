#include "deviceselector.h"
#include "ui_deviceselector.h"
#include <QDebug>

DeviceSelector::DeviceSelector(QWidget *parent)
    : QGroupBox(parent), ui(new Ui::DeviceSelector)
{
    ui->setupUi(this);
}

DeviceSelector::~DeviceSelector()
{
    delete ui;
}

void DeviceSelector::deviceSelect(DShowLib::Grabber *tmpGrabber)
{
    if (!(tmpGrabber->closeDev()))
    {
        qDebug() << "cannot close device properly "
                 << tmpGrabber->getDev().toString().c_str();
        return;
    }

    qDebug() << "opening: " << ui->comboDevs->currentText();

    if (!(tmpGrabber->openDevByUniqueName(
            ui->comboDevs->currentText().toStdString())))
    {
        qDebug() << "cannot open device " << ui->comboDevs->currentText();
        return;
    }
    if (!(tmpGrabber->isDevOpen()))
    {
        qDebug() << "device is not open " << ui->comboDevs->currentText();
        return;
    }
    if (!(tmpGrabber->isDevValid()))
    {
        qDebug() << "device invalid " << ui->comboDevs->currentText();
        return;
    }

    if (ui->comboNorms->isEnabled())
    {
        auto it = availableVideoNorms.find(ui->comboNorms->currentText());
        if (!tmpGrabber->setVideoNorm(it.value()))
        {
            qDebug() << "cannot set video norm "
                     << tmpGrabber->getLastError().toString().c_str();
            return;
        }
    }
    auto it     = availableVideoFormats.find(ui->comboFormats->currentText());
    auto size   = it.value()->getMaxSize();
    auto format = it.value()->createVideoFormat(size);
    if (!tmpGrabber->setVideoFormat(format))
    {
        qDebug() << "cannot set video format "
                 << tmpGrabber->getLastError().toString().c_str();
        return;
    }

    if (!tmpGrabber->setFPS(ui->comboFPS->currentText().toDouble()))
    {
        qDebug() << "cannot set fps "
                 << tmpGrabber->getLastError().toString().c_str();
        return;
    }
}

void DeviceSelector::devicesRefresh()
{
    auto list = grabber->getAvailableVideoCaptureDevices();
    ui->comboDevs->clear();
    availableDevices.clear();
    for (const auto &e : *list)
    {
        ui->comboDevs->addItem(QString::fromLatin1(e.getUniqueName().c_str()));
        availableDevices.insert(QString::fromLatin1(e.getUniqueName().c_str()),
                                e);
    }
}

void DeviceSelector::on_comboDevs_currentIndexChanged(const QString &arg1)
{
    if (arg1.isEmpty())
    {
        return;
    }
    if (!(grabber->closeDev()))
    {
        qDebug() << "cannot close device properly "
                 << grabber->getDev().toString().c_str();
        return;
    }
    if (!(grabber->openDevByUniqueName(arg1.toStdString())))
    {
        qDebug() << "cannot open device " << ui->comboDevs->currentText();
        return;
    }
    if (!(grabber->isDevOpen()))
    {
        qDebug() << "device is not open " << ui->comboDevs->currentText();
        return;
    }
    if (!(grabber->isDevValid()))
    {
        qDebug() << "device invalid " << ui->comboDevs->currentText();
        return;
    }
    ui->comboNorms->clear();
    availableVideoNorms.clear();
    if (grabber->isVideoNormAvailableWithCurDev())
    {
        ui->comboNorms->setEnabled(true);
        auto listNorms = grabber->getAvailableVideoNorms();
        if (!(listNorms.get()))
        {
            qDebug() << grabber->getLastError().toString().c_str();
            return;
        }
        for (const auto &n : *listNorms)
        {
            ui->comboNorms->addItem(QString::fromLatin1(n.toString().c_str()));
            availableVideoNorms.insert(
                QString::fromLatin1(n.toString().c_str()), n);
        }
    }
    else
    {
        ui->comboNorms->setEnabled(false);
        ui->comboFormats->clear();
        availableVideoFormats.clear();
        auto list = grabber->getAvailableVideoFormatDescs();
        if (!(list.get()))
        {
            qDebug() << grabber->getLastError().toString().c_str();
            return;
        }
        for (const auto &e : *list)
        {
            ui->comboFormats->addItem(
                QString::fromLatin1(e->toString().c_str()));
            availableVideoFormats.insert(
                QString::fromLatin1(e->toString().c_str()), e);
        }
    }
}

void DeviceSelector::on_comboNorms_currentIndexChanged(int index)
{
    if (!(grabber->isDevOpen()))
    {
        qDebug() << "device is not open " << ui->comboDevs->itemText(index);
        return;
    }
    if (!(grabber->isDevValid()))
    {
        qDebug() << "device invalid " << ui->comboDevs->itemText(index);
        return;
    }
    auto it   = availableVideoNorms.find(ui->comboNorms->itemText(index));
    auto list = grabber->getAvailableVideoFormatDescs(it.value());
    availableVideoFormats.clear();
    for (const auto &e : *list)
    {
        ui->comboFormats->addItem(QString::fromLatin1(e->toString().c_str()));
        availableVideoFormats.insert(QString::fromLatin1(e->toString().c_str()),
                                     e);
    }
}

void DeviceSelector::on_comboFormats_currentIndexChanged(int index)
{
    if (!(grabber->isDevOpen()))
    {
        qDebug() << "device is not open " << ui->comboDevs->itemText(index);
        return;
    }
    if (!(grabber->isDevValid()))
    {
        qDebug() << "device invalid " << ui->comboDevs->itemText(index);
        return;
    }
    grabber->setVideoFormat(ui->comboFormats->itemText(index).toStdString());
    ui->comboFPS->clear();
    auto list = grabber->getAvailableFPS(grabber->getVideoFormat());
    if (!(list.get()))
    {
        qDebug() << grabber->getLastError().toString().c_str();
        return;
    }
    for (const auto &e : *list)
    {
        ui->comboFPS->addItem(QString::number(e));
    }
}

void DeviceSelector::setGrabber(DShowLib::Grabber *value)
{
    grabber = value;
}

void DeviceSelector::on_pushButton_clicked()
{
    auto propItems = grabber->getAvailableVCDProperties();
    for (const auto &p : propItems->getItems())
    {
        qDebug() << "Name: " << p->getName().c_str();
        auto propElems = p->getElements();
        for (const auto &e : propElems)
        {
            qDebug() << "\tName: " << e->getName().c_str();
            auto elemInter = e->getInterfaces();
        }
    }
}
