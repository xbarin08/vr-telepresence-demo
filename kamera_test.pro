#-------------------------------------------------
#
# Project created by QtCreator 2019-04-23T16:57:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = kamera_test
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++14

SOURCES += \
        SimplePropertyAccess.cpp \
        cameraproperties.cpp \
        cframegrabber.cpp \
        chmd.cpp \
        cimagewindow.cpp \
        deviceselector.cpp \
        main.cpp \
        mainwindow.cpp \
        openglwidget.cpp \
        propertycontrollerbool.cpp \
        propertycontrollerint.cpp

HEADERS += \
        SimplePropertyAccess.h \
        cameraproperties.h \
        cframegrabber.h \
        chmd.h \
        cimagewindow.h \
        deviceselector.h \
        mainwindow.h \
        openglwidget.h \
        propertycontrollerbool.h \
        propertycontrollerint.h

FORMS += \
        cameraproperties.ui \
        cimagewindow.ui \
        deviceselector.ui \
        mainwindow.ui \
        propertycontrollerint.ui

win32 {
    INCLUDEPATH += 'C:/Users/martin/Documents/IC Imaging Control 3.4/classlib/include/'
    INCLUDEPATH += 'C:/Users/martin/Documents/vr/openvr/headers'
    LIBS += -L'C:/Users/martin/Documents/vr/openvr/bin/win64' -lopenvr_api
    LIBS += -L'C:/Users/martin/Documents/vr/openvr/lib/win64'
    LIBS += -L'C:/Users/martin/Documents/IC Imaging Control 3.4/classlib/x64/release/' -lTIS_UDSHL11_x64
    LIBS += -L'C:/Users/martin/Documents/IC Imaging Control 3.4/classlib/x64/debug/' -lTIS_UDSHL11d_x64
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
