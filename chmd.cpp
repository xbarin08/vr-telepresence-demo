#include "chmd.h"
#include <QtDebug>

CHMD::CHMD(QObject *parent) : QObject(parent) {}

CHMD::~CHMD()
{
    vr::VR_Shutdown();
}

void CHMD::initOpenVR()
{
    vr::EVRInitError error = vr::VRInitError_None;
    hmd                    = vr::VR_Init(&error, vr::VRApplication_Scene);

    if (error != vr::VRInitError_None)
    {
        QString s;
        s.append(QLatin1String("OpenVR Initialization Error: "));
        s.append(
            QString::fromLatin1(VR_GetVRInitErrorAsEnglishDescription(error)));
        s.append(QLatin1String("\n"));
        throw std::runtime_error(s.toStdString());
    }

    // Initialize the compositor
    compositor = vr::VRCompositor();
    if (!compositor)
    {
        vr::VR_Shutdown();
        QString s;
        s.append(QLatin1String("VR init failed\n"));
        throw std::runtime_error(s.toStdString());
    }
    updatePoses();
}

void CHMD::update()
{
    processVREvents();
    updatePoses();
}

void CHMD::submitLeftEye(GLuint target)
{
    submitImage(target, vr::EVREye::Eye_Left);
}

void CHMD::submitRightEye(GLuint target)
{
    submitImage(target, vr::EVREye::Eye_Right);
}

void CHMD::PostPresentHandoff()
{
    vr::VRCompositor()->PostPresentHandoff();
}

QSize CHMD::getResolution()
{
    unsigned w, h;
    hmd->GetRecommendedRenderTargetSize(&w, &h);
    return QSize(w, h);
}

void CHMD::processVREvents()
{
    vr::VREvent_t event{};
    while (hmd->PollNextEvent(&event, sizeof(event))) {};
}

void CHMD::updatePoses()
{
    vr::VRCompositor()->WaitGetPoses(trackedDevicePose,
                                     vr::k_unMaxTrackedDeviceCount, nullptr, 0);
}

void CHMD::submitImage(GLuint target, vr::EVREye eye)
{
    const vr::Texture_t tex = {reinterpret_cast<void *>(intptr_t(target)),
                               vr::TextureType_OpenGL, vr::ColorSpace_Gamma};

    vr::EVRCompositorError error = vr::VRCompositor()->Submit(
        eye, &tex, nullptr, vr::EVRSubmitFlags::Submit_Default);

    if (error == vr::EVRCompositorError::VRCompositorError_DoNotHaveFocus)
    {
        // return;
    }

    if (error != vr::EVRCompositorError::VRCompositorError_None)
    {
        QString s;
        s.append(QLatin1String("EVRCompositorError::Submit error "));
        s.append(QString::number(error));
        s.append('\n');
        //        throw std::runtime_error(s.toStdString());
        qDebug() << eye << " " << s;
    }
}
