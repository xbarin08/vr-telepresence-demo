#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QMessageBox>
#include <QThread>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    tmpGrabberLeft  = std::make_unique<DShowLib::Grabber>();
    tmpGrabberRight = std::make_unique<DShowLib::Grabber>();
    grabberLeft     = std::make_unique<DShowLib::Grabber>();
    grabberRight    = std::make_unique<DShowLib::Grabber>();
    ui->devsLeft->setGrabber(tmpGrabberLeft.get());
    ui->devsRight->setGrabber(tmpGrabberRight.get());
    ui->tabWidget->setCurrentIndex(0);
    ui->devsLeft->devicesRefresh();
    ui->devsRight->devicesRefresh();
    connect(ui->buttonDevsRefresh, &QPushButton::clicked, ui->devsLeft,
            &DeviceSelector::devicesRefresh);
    connect(ui->buttonDevsRefresh, &QPushButton::clicked, ui->devsRight,
            &DeviceSelector::devicesRefresh);
    propertiesRefreshTimer.setInterval(1000);
    connect(&propertiesRefreshTimer, &QTimer::timeout, this,
            &MainWindow::on_propertiesRefreshTimer_timeout);
    connect(&window, &CImageWindow::stop, this,
            &MainWindow::on_pushStop_clicked);
}

MainWindow::~MainWindow()
{
    delete ui;
    tmpGrabberLeft->closeDev();
    tmpGrabberRight->closeDev();
    grabberLeft->closeDev();
    grabberRight->closeDev();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    on_pushStop_clicked();
    QMainWindow::closeEvent(event);
}

void MainWindow::on_buttonSelect_clicked()
{
    if (!(grabberLeft->closeDev()))
    {
        qDebug() << "cannot close device: "
                 << grabberLeft->getDev().toString().c_str() << " : "
                 << grabberLeft->getLastError().toString().c_str();
        return;
    }

    if (!(grabberRight->closeDev()))
    {
        qDebug() << "cannot close device: "
                 << grabberRight->getDev().toString().c_str() << " : "
                 << grabberRight->getLastError().toString().c_str();
        return;
    }

    if (!(tmpGrabberLeft->closeDev()))
    {
        qDebug() << "cannot close device: "
                 << tmpGrabberLeft->getDev().toString().c_str() << " : "
                 << tmpGrabberLeft->getLastError().toString().c_str();
        return;
    }

    if (!(tmpGrabberRight->closeDev()))
    {
        qDebug() << "cannot close device: "
                 << tmpGrabberRight->getDev().toString().c_str() << " : "
                 << tmpGrabberRight->getLastError().toString().c_str();
        return;
    }

    ui->devsLeft->deviceSelect(tmpGrabberLeft.get());
    ui->propertiesLeft->loadProperties(tmpGrabberLeft.get());

    ui->devsRight->deviceSelect(tmpGrabberRight.get());
    ui->propertiesRight->loadProperties(tmpGrabberRight.get());

    grabberLeft.swap(tmpGrabberLeft);
    grabberRight.swap(tmpGrabberRight);

    tmpGrabberLeft->closeDev();
    tmpGrabberRight->closeDev();

    ui->tabWidget->setCurrentIndex(1);
    ui->pushStart->setEnabled(true);
}

void MainWindow::on_buttonPropertiesApply_clicked()
{
    ui->propertiesLeft->applyProperties(grabberLeft.get());
    ui->propertiesRight->applyProperties(grabberRight.get());
}

void MainWindow::on_pushStart_clicked()
{
    grabberLeft->addListener(window.getGrabberLeft(),
                             DShowLib::GrabberListener::eFRAMEREADY);
    auto acceptedTypesLeft =
        DShowLib::FrameTypeInfoArray::createOptimalTypesArray();
    frameSinkLeft = DShowLib::FrameHandlerSink::create(acceptedTypesLeft, 5);
    frameSinkLeft->setSnapMode(false);
    grabberLeft->setSinkType(frameSinkLeft);
    grabberLeft->startLive(false);

    grabberRight->addListener(window.getGrabberRight(),
                              DShowLib::GrabberListener::eFRAMEREADY);
    auto acceptedTypesRight =
        DShowLib::FrameTypeInfoArray::createOptimalTypesArray();
    frameSinkRight = DShowLib::FrameHandlerSink::create(acceptedTypesRight, 5);
    frameSinkRight->setSnapMode(false);
    grabberRight->setSinkType(frameSinkRight);
    grabberRight->startLive(false);

    window.show();
    ui->pushStart->setEnabled(false);
    ui->pushStop->setEnabled(true);
    propertiesRefreshTimer.start();
}

void MainWindow::on_pushStop_clicked()
{
    window.hide();
    propertiesRefreshTimer.stop();
    grabberLeft->stopLive();
    grabberLeft->removeListener(window.getGrabberLeft());

    grabberRight->stopLive();
    grabberRight->removeListener(window.getGrabberRight());

    while (grabberLeft->isListenerRegistered(window.getGrabberLeft()))
    {
        QThread::usleep(1);
    }

    while (grabberRight->isListenerRegistered(window.getGrabberRight()))
    {
        QThread::usleep(1);
    }

    ui->pushStart->setEnabled(true);
    ui->pushStop->setEnabled(false);
}

void MainWindow::on_propertiesRefreshTimer_timeout()
{
    ui->propertiesLeft->refreshProperties(grabberLeft.get());
    ui->propertiesRight->refreshProperties(grabberRight.get());
}
