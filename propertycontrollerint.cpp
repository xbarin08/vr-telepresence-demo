#include "propertycontrollerint.h"
#include "SimplePropertyAccess.h"
#include "ui_propertycontrollerint.h"
#include <QDebug>

PropertyControllerInt::PropertyControllerInt(QWidget *parent)
    : QGroupBox(parent), ui(new Ui::PropertyControllerInt)
{
    ui->setupUi(this);
}

PropertyControllerInt::~PropertyControllerInt()
{
    delete ui;
}

int PropertyControllerInt::getValue() const
{
    return ui->spinBox->value();
}

void PropertyControllerInt::setValue(int value)
{
    ui->spinBox->setValue(value);
    ui->horizontalSlider->setValue(value);
    emit valueChanged(value);
}

void PropertyControllerInt::setRange(int min, int max)
{
    ui->spinBox->setRange(min, max);
    ui->horizontalSlider->setRange(min, max);
}

void PropertyControllerInt::setAutoPossible(bool b)
{
    ui->checkBox->setEnabled(b);
}

void PropertyControllerInt::setPossible(bool b)
{
    this->setEnabled(b);
}

void PropertyControllerInt::setAuto(bool b)
{
    ui->checkBox->setChecked(b);
    ui->spinBox->setEnabled(!b);
    ui->horizontalSlider->setEnabled(!b);
}

bool PropertyControllerInt::getAuto() const
{
    return ui->checkBox->isChecked();
}

void PropertyControllerInt::loadProperties(DShowLib::Grabber *grabber, GUID id)
{
    if (!(grabber->getAvailableVCDProperties().get()))
    {
        qDebug() << "cannot get VCD properties: "
                 << grabber->getDev().toString().c_str() << " "
                 << grabber->getLastError().toString().c_str();
        return;
    }
    CSimplePropertyAccess prop(grabber->getAvailableVCDProperties());
    if (prop.isAvailable(id))
    {
        setPossible(true);
        setAutoPossible(prop.isAutoAvailable(id));
        setAuto(prop.getAuto(id));
        setRange(prop.getRangeMin(id), prop.getRangeMax(id));
        setValue(prop.getValue(id));
    }
    else
    {
        setPossible(false);
    }
}

void PropertyControllerInt::refreshProperties(DShowLib::Grabber *grabber,
                                              GUID id)
{
    CSimplePropertyAccess prop(grabber->getAvailableVCDProperties());
    if (prop.isAvailable(id))
    {
        if (prop.getAuto(id))
        {
            setAutoPossible(prop.isAutoAvailable(id));
            setAuto(prop.getAuto(id));
            setRange(prop.getRangeMin(id), prop.getRangeMax(id));
            setValue(prop.getValue(id));
        }
    }
}

void PropertyControllerInt::applyProperties(DShowLib::Grabber *grabber, GUID id)
{
    if (!grabber->getAvailableVCDProperties().get())
    {
        qDebug() << "cannot load VCD properties: "
                 << grabber->getLastError().toString().c_str();
        return;
    }
    CSimplePropertyAccess prop(grabber->getAvailableVCDProperties());
    if (!prop.isAvailable(id))
    {
        return;
    }
    if (prop.isAutoAvailable(id))
    {
        prop.setAuto(id, ui->checkBox->isChecked());
    }
    else
    {
        prop.setValue(id, ui->spinBox->value());
    }
}
