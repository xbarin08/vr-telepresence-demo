#ifndef CFRAMEGRABBER_H
#define CFRAMEGRABBER_H

#include "tisudshl.h"
#include <QImage>

class CFrameGrabber : public DShowLib::GrabberListener
{
public:
    CFrameGrabber(QImage *const img, std::function<void(void)> callback);

    void frameReady(DShowLib::Grabber &caller,
                    smart_ptr<DShowLib::MemBuffer> buffer,
                    DWORD FrameNumber) override;

private:
    QImage *const image{nullptr};

    std::function<void(void)> callback;
};

#endif // CFRAMEGRABBER_H
