#include "mainwindow.h"
#include <QApplication>
#include <QSurfaceFormat>

int main(int argc, char *argv[])
{
    QSurfaceFormat tmpFormat = QSurfaceFormat::defaultFormat();
    tmpFormat.setRenderableType(QSurfaceFormat::OpenGL);
    tmpFormat.setVersion(3, 3);
    tmpFormat.setProfile(QSurfaceFormat::CoreProfile);
#ifdef QT_DEBUG
    tmpFormat.setOption(QSurfaceFormat::FormatOption::DebugContext, true);
#endif
    QSurfaceFormat::setDefaultFormat(tmpFormat);

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return QApplication::exec();
}
