#ifndef CIMAGEWINDOW_H
#define CIMAGEWINDOW_H

#include "cframegrabber.h"
#include "tisudshl.h"
#include <QPainter>
#include <QWidget>
#include <chmd.h>
#include <functional>

namespace Ui
{
class CImageWindow;
}

class CImageWindow : public QWidget
{
    Q_OBJECT

public:
    explicit CImageWindow(QWidget *parent = nullptr);
    ~CImageWindow() override;

    CFrameGrabber *getGrabberLeft();
    CFrameGrabber *getGrabberRight();

signals:
    void stop();

private slots:
    void on_pushStop_clicked();

    void on_widget_resize();

private:
    void leftFrameReady();

    void rightFrameReady();

    Ui::CImageWindow *ui;
    QImage imageLeft;
    QImage imageRight;
    QSize oldSize{};

    CFrameGrabber grabberLeft;
    CFrameGrabber grabberRight;

    CHMD vr;
};

#endif // CIMAGEWINDOW_H
