#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include "chmd.h"
#include <QMutex>
#include <QOpenGLBuffer>
#include <QOpenGLDebugLogger>
#include <QOpenGLDebugMessage>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLWidget>
#include <QWidget>

class OpenGLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit OpenGLWidget(QWidget *parent = nullptr);
    ~OpenGLWidget() override;

    void updateTextureLeft(QImage *img);
    void updateTextureRight(QImage *img);

    QSize sizeHint() const override;

    QSize minimumSizeHint() const override;

    void initFrameBuffers(QSize size);

    void setVr(CHMD *value);

private slots:

    void on_logger_logged(const QOpenGLDebugMessage &message);

protected:
    void paintGL() override;
    void resizeGL(int w, int h) override;
    void initializeGL() override;

    void loadTexture(const QImage &img, const QOpenGLTexture &tex);

    QOpenGLShaderProgram shader;
    QOpenGLBuffer vbo{QOpenGLBuffer::VertexBuffer};
    QOpenGLVertexArrayObject vao;
    QOpenGLTexture texLeft{QOpenGLTexture::Target2D};
    QOpenGLTexture texRight{QOpenGLTexture::Target2D};
    QOpenGLDebugLogger logger{this};
    std::unique_ptr<QOpenGLFramebufferObject> fboLeft;
    std::unique_ptr<QOpenGLFramebufferObject> fboRight;

    int MVPUL{0};
    QMatrix4x4 vrTransform;
    int texUL{0};
    QSize newSize{};
    QSize oldSize{};

    QImage tmpImageLeft;
    QImage tmpImageRight;
    bool toUpdate{true};

    QMutex mutex;

    CHMD *vr{nullptr};
};

#endif // OPENGLWIDGET_H
