
#define _WIN32_WINNT 0x0500

#include "SimplePropertyAccess.h"

using namespace _DSHOWLIB_NAMESPACE;

CSimplePropertyAccess::CSimplePropertyAccess() : m_pItemContainer(nullptr) {}

CSimplePropertyAccess::CSimplePropertyAccess(
    const _DSHOWLIB_NAMESPACE::tIVCDPropertyItemsPtr &pItems)
    : m_pItemContainer(pItems)
{
}

CSimplePropertyAccess::~CSimplePropertyAccess() = default;

void CSimplePropertyAccess::init(
    const _DSHOWLIB_NAMESPACE::tIVCDPropertyItemsPtr &pItems)
{
    m_pItemContainer = pItems;
}

tIVCDRangePropertyPtr
getRangeInterface(const _DSHOWLIB_NAMESPACE::tIVCDPropertyItemsPtr &pItems,
                  GUID id)
{
    GUID itemID = id;
    GUID elemID = VCDElement_Value;

    if (itemID == VCDElement_WhiteBalanceRed ||
        itemID == VCDElement_WhiteBalanceBlue)
    {
        elemID = itemID;
        itemID = VCDID_WhiteBalance;
    }

    if (itemID == VCDElement_GPIOIn || itemID == VCDElement_GPIOOut)
    {
        elemID = itemID;
        itemID = VCDID_GPIO;
    }

    if (itemID == VCDElement_StrobeDelay || itemID == VCDElement_StrobeDuration)
    {
        elemID = itemID;
        itemID = VCDID_Strobe;
    }

    tIVCDPropertyElementPtr pFoundElement = pItems->findElement(itemID, elemID);
    if (pFoundElement != nullptr)
    {
        tIVCDRangePropertyPtr pRange;
        if (pFoundElement->getInterfacePtr(pRange) != nullptr)
        {
            return pRange;
        }
    }
    return nullptr;
}

tIVCDSwitchPropertyPtr
getAutoInterface(const _DSHOWLIB_NAMESPACE::tIVCDPropertyItemsPtr &pItems,
                 GUID id)
{
    tIVCDPropertyElementPtr pFoundElement =
        pItems->findElement(id, VCDElement_Auto);
    if (pFoundElement != nullptr)
    {
        tIVCDSwitchPropertyPtr pAuto;
        if (pFoundElement->getInterfacePtr(pAuto) != nullptr)
        {
            return pAuto;
        }
    }
    return nullptr;
}

tIVCDButtonPropertyPtr
getOnePushInterface(const _DSHOWLIB_NAMESPACE::tIVCDPropertyItemsPtr &pItems,
                    GUID id)
{
    GUID itemID = id;
    GUID elemID = VCDElement_OnePush;

    if (itemID == VCDElement_GPIORead || itemID == VCDElement_GPIOWrite)
    {
        elemID = itemID;
        itemID = VCDID_GPIO;
    }

    tIVCDPropertyElementPtr pFoundElement = pItems->findElement(itemID, elemID);
    if (pFoundElement != nullptr)
    {
        tIVCDButtonPropertyPtr pOnePush;
        if (pFoundElement->getInterfacePtr(pOnePush) != nullptr)
        {
            return pOnePush;
        }
    }
    return nullptr;
}

bool CSimplePropertyAccess::isAvailable(GUID id)
{
    assert(m_pItemContainer != nullptr);

    if (id == VCDElement_WhiteBalanceRed || id == VCDElement_WhiteBalanceBlue)
    {
        return m_pItemContainer->findElement(VCDID_WhiteBalance, id) != nullptr;
    }
    else
    {
        return m_pItemContainer->findItem(id) != nullptr;
    }
}

long CSimplePropertyAccess::getValue(GUID id)
{
    assert(m_pItemContainer != nullptr);

    long rval                    = 0;
    tIVCDRangePropertyPtr pRange = getRangeInterface(m_pItemContainer, id);
    if (pRange != nullptr)
    {
        rval = pRange->getValue();
    }
    return rval;
}

void CSimplePropertyAccess::setValue(GUID id, long val)
{
    assert(m_pItemContainer != nullptr);

    tIVCDRangePropertyPtr pRange = getRangeInterface(m_pItemContainer, id);
    if (pRange != nullptr)
    {
        pRange->setValue(val);
    }
}

long CSimplePropertyAccess::getRangeMin(GUID id)
{
    assert(m_pItemContainer != nullptr);

    long rval                    = 0;
    tIVCDRangePropertyPtr pRange = getRangeInterface(m_pItemContainer, id);
    if (pRange != nullptr)
    {
        rval = pRange->getRangeMin();
    }
    return rval;
}

long CSimplePropertyAccess::getRangeMax(GUID id)
{
    assert(m_pItemContainer != nullptr);

    long rval                    = 0;
    tIVCDRangePropertyPtr pRange = getRangeInterface(m_pItemContainer, id);
    if (pRange != nullptr)
    {
        rval = pRange->getRangeMax();
    }
    return rval;
}

long CSimplePropertyAccess::getDefault(GUID id)
{
    assert(m_pItemContainer != nullptr);

    long rval                    = 0;
    tIVCDRangePropertyPtr pRange = getRangeInterface(m_pItemContainer, id);
    if (pRange != nullptr)
    {
        rval = pRange->getDefault();
    }
    return rval;
}

bool CSimplePropertyAccess::isAutoAvailable(GUID id)
{
    assert(m_pItemContainer != nullptr);

    return getAutoInterface(m_pItemContainer, id) != nullptr;
}

bool CSimplePropertyAccess::getAuto(GUID id)
{
    assert(m_pItemContainer != nullptr);

    bool rval                    = false;
    tIVCDSwitchPropertyPtr pAuto = getAutoInterface(m_pItemContainer, id);
    if (pAuto != nullptr)
    {
        rval = pAuto->getSwitch();
    }
    return rval;
}

void CSimplePropertyAccess::setAuto(GUID id, bool b)
{
    assert(m_pItemContainer != nullptr);

    tIVCDSwitchPropertyPtr pAuto = getAutoInterface(m_pItemContainer, id);
    if (pAuto != nullptr)
    {
        pAuto->setSwitch(b);
    }
}

bool CSimplePropertyAccess::isOnePushAvailable(GUID id)
{
    assert(m_pItemContainer != nullptr);

    tIVCDButtonPropertyPtr pOnePush = getOnePushInterface(m_pItemContainer, id);
    return pOnePush != nullptr;
}

void CSimplePropertyAccess::push(GUID id)
{
    assert(m_pItemContainer != nullptr);

    tIVCDButtonPropertyPtr pOnePush = getOnePushInterface(m_pItemContainer, id);
    if (pOnePush != nullptr)
    {
        pOnePush->push();
    }
}

bool CSimplePropertyAccess::isSwitchAvailable(GUID id)
{
    return isAutoAvailable(id);
}

bool CSimplePropertyAccess::getSwitch(GUID id)
{
    return getAuto(id);
}

void CSimplePropertyAccess::setSwitch(GUID id, bool b)
{
    setAuto(id, b);
}
