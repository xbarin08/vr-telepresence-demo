#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "cimagewindow.h"
#include "tisudshl.h"
#include <QMainWindow>
#include <QMap>
#include <QTimer>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

private slots:

    void on_buttonSelect_clicked();

    void on_buttonPropertiesApply_clicked();

    void on_pushStart_clicked();

    void on_pushStop_clicked();

    void on_propertiesRefreshTimer_timeout();

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    Ui::MainWindow *ui;
    std::unique_ptr<DShowLib::Grabber> grabberLeft;
    std::unique_ptr<DShowLib::Grabber> grabberRight;
    std::unique_ptr<DShowLib::Grabber> tmpGrabberLeft;
    std::unique_ptr<DShowLib::Grabber> tmpGrabberRight;

    QMap<QString, DShowLib::VideoCaptureDeviceItem> availableDevices;
    QMap<QString, DShowLib::VideoNormItem> availableVideoNorms;
    QMap<QString, smart_ptr<DShowLib::VideoFormatDesc>> availableVideoFormats;

    CImageWindow window;

    smart_ptr<DShowLib::FrameHandlerSink> frameSinkLeft;
    smart_ptr<DShowLib::FrameHandlerSink> frameSinkRight;

    QTimer propertiesRefreshTimer;
};

#endif // MAINWINDOW_H
